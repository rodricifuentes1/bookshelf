package com.example.bookshelf.domain;

import com.example.bookshelf.domain.entities.AbstractBook;
import com.example.bookshelf.mapper.AuthorMapper;
import com.example.bookshelf.mapper.BookMapper;
import com.example.bookshelf.persistence.AuthorBookDAO;
import com.example.bookshelf.persistence.AuthorDAO;
import com.example.bookshelf.persistence.BookDAO;
import com.example.bookshelf.persistence.Tables;
import com.example.bookshelf.persistence.tables.records.AuthorBookRecord;
import com.example.bookshelf.persistence.tables.records.AuthorRecord;
import com.example.bookshelf.persistence.tables.records.BookRecord;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class Bookshelf {

    private final AuthorDAO authorDAO;
    private final AuthorBookDAO authorBookDAO;
    private final BookDAO bookDAO;

    public Bookshelf() {
        this.authorDAO = new AuthorDAO();
        this.authorBookDAO = new AuthorBookDAO();
        this.bookDAO = new BookDAO();
    }

    public CompletableFuture<List<AbstractBook>> getAllBooks(final DSLContext dbContext) {
        return CompletableFuture
                .supplyAsync(() -> {
                    return dbContext
                            .select(
                                    Tables.BOOK.ID,
                                    Tables.BOOK.ISBN,
                                    Tables.BOOK.TITLE,
                                    Tables.BOOK.SUBTITLE,
                                    Tables.BOOK.PUBLISHED,
                                    Tables.BOOK.PUBLISHER,
                                    Tables.BOOK.PAGES,
                                    Tables.BOOK.DESCRIPTION,
                                    Tables.BOOK.INSTOCK,
                                    DSL.groupConcat(DSL.concat(Tables.AUTHOR.NAME, DSL.val(" "), Tables.AUTHOR.LAST_NAME)).as("authors")
                            )
                            .from(
                                    Tables.BOOK
                                            .innerJoin(Tables.AUTHOR_BOOK).on(Tables.BOOK.ID.equal(Tables.AUTHOR_BOOK.BOOK_ID))
                                            .innerJoin(Tables.AUTHOR).on(Tables.AUTHOR.ID.equal(Tables.AUTHOR_BOOK.AUTHOR_ID))
                            )
                            .groupBy(Tables.BOOK.ID)
                            .fetch()
                            .map(BookMapper::record2Abstract);
                });
    }

    public CompletionStage<String> storeBook(final AbstractBook book, final DSLContext dbContext) {
        return dbContext.transactionResultAsync(configuration -> {
            // create book, authors records
            BookRecord bookRecord = BookMapper.abstract2record(book);
            List<AuthorRecord> authorRecords = book.authors()
                    .stream()
                    .map(AuthorMapper::abstract2Record)
                    .collect(Collectors.toList());

            // insert book, authors
            BookRecord createdBook = bookDAO.store(bookRecord, configuration);
            List<AuthorRecord> createdAuthors = authorDAO.store(authorRecords, configuration);

            // create author-book relationship records
            List<AuthorBookRecord> authorBookRecords = createdAuthors.stream()
                    .map(author -> new AuthorBookRecord(author.getId(), createdBook.getId()))
                    .collect(Collectors.toList());

            // insert author-book relationship
            authorBookDAO.store(authorBookRecords, configuration);

            // return book id
            return createdBook.getId();
        });
    }

    public CompletableFuture<Optional<AbstractBook>> getBookById(final String bookId, final DSLContext dbContext) {
        return CompletableFuture
                .supplyAsync(() -> {
                    return dbContext
                            .select(
                                    Tables.BOOK.ID,
                                    Tables.BOOK.ISBN,
                                    Tables.BOOK.TITLE,
                                    Tables.BOOK.SUBTITLE,
                                    Tables.BOOK.PUBLISHED,
                                    Tables.BOOK.PUBLISHER,
                                    Tables.BOOK.PAGES,
                                    Tables.BOOK.DESCRIPTION,
                                    Tables.BOOK.INSTOCK,
                                    DSL.groupConcat(DSL.concat(Tables.AUTHOR.NAME, DSL.val(" "), Tables.AUTHOR.LAST_NAME)).as("authors")
                            )
                            .from(
                                    Tables.BOOK
                                            .innerJoin(Tables.AUTHOR_BOOK).on(Tables.BOOK.ID.equal(Tables.AUTHOR_BOOK.BOOK_ID))
                                            .innerJoin(Tables.AUTHOR).on(Tables.AUTHOR.ID.equal(Tables.AUTHOR_BOOK.AUTHOR_ID))
                            )
                            .where(Tables.BOOK.ID.equal(bookId))
                            .groupBy(Tables.BOOK.ID)
                            .fetchOptional()
                            .map(BookMapper::record2Abstract);
                });
    }

    public CompletionStage<Void> deleteBookById(final String bookId, final DSLContext dbContext) {
        return dbContext.transactionAsync(configuration -> {
            authorBookDAO.deleteByBookId(bookId, configuration);
            bookDAO.deleteById(bookId, configuration);
        });
    }
}
