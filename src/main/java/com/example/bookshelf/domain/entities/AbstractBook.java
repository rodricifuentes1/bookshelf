package com.example.bookshelf.domain.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.List;
import java.util.Optional;

@Value.Immutable
@Value.Style(typeImmutable = "*")
@JsonDeserialize(as = Book.class)
@JsonSerialize(as = Book.class)
public interface AbstractBook {
    Optional<String> id();
    String isbn();
    String title();
    String subtitle();
    String published();
    String publisher();
    Integer pages();
    String description();
    Boolean instock();
    List<AbstractAuthor> authors();
}
