package com.example.bookshelf.domain.entities;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class AbstractAuthorSerializer extends JsonSerializer<AbstractAuthor> {
    @Override
    public void serialize(AbstractAuthor value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        String completeName = value.firstName() + " " + value.lastName();
        gen.writeString(completeName);
    }
}
