package com.example.bookshelf.domain.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(typeImmutable = "*")
@JsonDeserialize(as = Author.class)
@JsonSerialize(using = AbstractAuthorSerializer.class, as = Author.class)
public interface AbstractAuthor {
    String firstName();
    String lastName();
}
