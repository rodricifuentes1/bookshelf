package com.example.bookshelf.api;

import com.example.bookshelf.domain.Bookshelf;
import com.example.bookshelf.domain.entities.AbstractBook;
import io.swagger.annotations.*;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

@Path("/bookshelf")
@Api(value = "/bookshelf")
public class BookshelfResource {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Bookshelf bookshelf;

    public BookshelfResource() {
        this.bookshelf = new Bookshelf();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Get all books in the bookshelf",
            response = AbstractBook.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "An exception occurred executing requested operation")
    })
    public void getAllBooks(@Suspended AsyncResponse response, @Context DSLContext dbContext) {
        bookshelf
                .getAllBooks(dbContext)
                .thenApplyAsync(books -> response.resume(Response.ok(books).build()))
                .exceptionally(exception -> {
                    logger.error("there was an exception querying bookshelf books", exception);
                    return response.resume(exception);
                });
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            httpMethod = "POST",
            consumes = "application/json",
            value = "Store a book in the bookshelf"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "OK", responseHeaders = {@ResponseHeader(name = "Location", description = "Location where books was stored")}),
            @ApiResponse(code = 500, message = "An exception occurred executing requested operation")
    })
    public void saveBook(
            @ApiParam(value = "Book object that needs to be added to the bookshelf", required = true) AbstractBook book,
            @Suspended AsyncResponse response, @Context DSLContext dbContext, @Context UriInfo uriInfo
    ) {
        bookshelf
                .storeBook(book, dbContext)
                .thenApplyAsync(bookId -> {
                    URI locationUri = uriInfo.getAbsolutePathBuilder().path(bookId).build();
                    return response.resume(Response.created(locationUri).build());
                })
                .exceptionally(exception -> {
                    logger.error("there was an exception saving book", exception);
                    return response.resume(exception);
                });
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Get the details of a book in the bookshelf",
            response = AbstractBook.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Book does not exist"),
            @ApiResponse(code = 500, message = "An exception occurred executing requested operation")
    })
    public void getBookById(
            @ApiParam(value = "the id of the book to retrieve details") @PathParam("id") String bookId,
            @Suspended AsyncResponse response, @Context DSLContext dbContext
    ) {
        bookshelf
                .getBookById(bookId, dbContext)
                .thenApplyAsync(bookOption ->
                        bookOption
                                .map(book -> response.resume(Response.ok(book).build()))
                                .orElse(response.resume(Response.status(404).build()))
                )
                .exceptionally(exception -> {
                    logger.error("there was an exception getting book by id", exception);
                    return response.resume(exception);
                });
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Deletes a book from the bookshelf given its id"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "OK"),
            @ApiResponse(code = 500, message = "An exception occurred executing requested operation")
    })
    public void deleteBookById(
            @ApiParam(value = "the id of the book to delete") @PathParam("id") String bookId,
            @Suspended AsyncResponse response, @Context DSLContext dbContext
    ) {
        bookshelf
                .deleteBookById(bookId, dbContext)
                .thenApplyAsync(x -> response.resume(Response.noContent().build()))
                .exceptionally(exception -> {
                    logger.error("there was an exception getting book by id", exception);
                    return response.resume(exception);
                });
    }
}
