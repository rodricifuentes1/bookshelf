package com.example.bookshelf.persistence;

import com.example.bookshelf.persistence.tables.records.AuthorBookRecord;
import org.jooq.Configuration;
import org.jooq.impl.DSL;

import java.util.List;
import java.util.stream.Collectors;

public class AuthorBookDAO {

    public List<AuthorBookRecord> store(final List<AuthorBookRecord> records, final Configuration config) {
        return records.stream()
                .map(record -> store(record, config))
                .collect(Collectors.toList());
    }

    public AuthorBookRecord store(final AuthorBookRecord record, final Configuration config) {
        return DSL.using(config)
                .insertInto(Tables.AUTHOR_BOOK)
                .set(record)
                .returning()
                .fetchOne();
    }

    public int deleteByBookId(final String bookId, final Configuration config) {
        return DSL.using(config)
                .deleteFrom(Tables.AUTHOR_BOOK)
                .where(Tables.AUTHOR_BOOK.BOOK_ID.equal(bookId))
                .execute();
    }
}
