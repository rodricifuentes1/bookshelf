package com.example.bookshelf.persistence;

import com.example.bookshelf.persistence.tables.records.AuthorRecord;
import org.jooq.Configuration;
import org.jooq.impl.DSL;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class AuthorDAO {

    public List<AuthorRecord> store(final List<AuthorRecord> records, final Configuration config) {
        return records
                .stream()
                .map(record -> store(record, config))
                .collect(Collectors.toList());
    }

    public AuthorRecord store(final AuthorRecord record, final Configuration config) {
        return DSL.using(config)
                .insertInto(Tables.AUTHOR)
                .set(record.setId(UUID.randomUUID().toString()))
                .returning()
                .fetchOne();
    }
}
