package com.example.bookshelf.persistence;

import com.example.bookshelf.persistence.tables.records.BookRecord;
import org.jooq.Configuration;
import org.jooq.impl.DSL;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class BookDAO {

    public List<BookRecord> store(final List<BookRecord> records, final Configuration config) {
        return records
                .stream()
                .map(record -> store(record, config))
                .collect(Collectors.toList());
    }

    public BookRecord store(final BookRecord record, final Configuration config) {
        return DSL.using(config)
                .insertInto(Tables.BOOK)
                .set(record.setId(UUID.randomUUID().toString()))
                .returning()
                .fetchOne();
    }

    public int deleteById(final String bookId, final Configuration config) {
        return DSL.using(config)
                .deleteFrom(Tables.BOOK)
                .where(Tables.BOOK.ID.equal(bookId))
                .execute();
    }
}
