package com.example.bookshelf;

import com.bendb.dropwizard.jooq.JooqBundle;
import com.example.bookshelf.api.BookshelfResource;
import io.dropwizard.Application;
import io.dropwizard.configuration.ResourceConfigurationSourceProvider;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookshelfApp extends Application<BookshelfConfig> {

    private static Logger logger = LoggerFactory.getLogger(BookshelfApp.class);

    public static void main(String[] args) throws Exception {
        new BookshelfApp().run(args);
    }

    @Override
    public void initialize(Bootstrap<BookshelfConfig> bootstrap) {
        bootstrap.setConfigurationSourceProvider(new ResourceConfigurationSourceProvider());
        bootstrap.addBundle(new JooqBundle<BookshelfConfig>() {
            @Override
            public PooledDataSourceFactory getDataSourceFactory(BookshelfConfig configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(new SwaggerBundle<BookshelfConfig>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(BookshelfConfig configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });
    }

    @Override
    public void run(BookshelfConfig configuration, Environment environment) throws Exception {
        logger.info("running {} application ...", configuration.getAppName());
        environment.jersey().register(new JsonProcessingExceptionMapper(true));
        environment.jersey().register(new BookshelfResource());
    }
}
