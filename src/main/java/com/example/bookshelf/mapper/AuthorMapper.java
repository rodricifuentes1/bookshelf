package com.example.bookshelf.mapper;

import com.example.bookshelf.domain.entities.AbstractAuthor;
import com.example.bookshelf.domain.entities.Author;
import com.example.bookshelf.persistence.tables.records.AuthorRecord;

public class AuthorMapper {

    public static AuthorRecord abstract2Record(AbstractAuthor author) {
        return new AuthorRecord().setName(author.firstName()).setLastName(author.lastName());
    }

    public static AbstractAuthor record2abstract(AuthorRecord record) {
        return Author.builder().firstName(record.getName()).lastName(record.getLastName()).build();
    }
}
