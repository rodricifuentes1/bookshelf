package com.example.bookshelf.mapper;

import com.example.bookshelf.domain.entities.AbstractBook;
import com.example.bookshelf.domain.entities.Author;
import com.example.bookshelf.domain.entities.Book;
import com.example.bookshelf.persistence.tables.records.BookRecord;
import org.jooq.Record10;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BookMapper {

    public static BookRecord abstract2record(AbstractBook book) {
        return new BookRecord()
                .setIsbn(book.isbn())
                .setTitle(book.title())
                .setSubtitle(book.subtitle())
                .setPublished(book.published())
                .setPublisher(book.publisher())
                .setPages(book.pages())
                .setDescription(book.description())
                .setInstock(book.instock() ? 1 : 0);
    }

    public static AbstractBook record2Abstract(Record10<String, String, String, String, String, String, Integer, String, Integer, String> record) {
        List<Author> authors = Arrays.stream(record.component10().split(","))
                .map(fullName -> {
                    int idx = fullName.lastIndexOf(' ');
                    String firstName = fullName.substring(0, idx);
                    String lastName = fullName.substring(idx + 1);
                    return Author.builder().firstName(firstName).lastName(lastName).build();
                })
                .collect(Collectors.toList());

        return Book.builder()
                .id(record.component1())
                .isbn(record.component2())
                .title(record.component3())
                .subtitle(record.component4())
                .published(record.component5())
                .publisher(record.component6())
                .pages(record.component7())
                .description(record.component8())
                .instock(record.component9() == 1)
                .addAllAuthors(authors)
                .build();
    }
}
